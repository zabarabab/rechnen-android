## Rechnen-Android

(Rechnen = german for calculating)

This is a simple, but configurable mental calculation trainer.

Download: <https://f-droid.org/en/packages/com.rechnen.app/>

### Screenshots

![Screenshot of training itself](./app/src/main/play/en-US/listing/phoneScreenshots/1.png)

![Screenshot of a part of the configuration screen](./app/src/main/play/en-US/listing/phoneScreenshots/2.png)

### History

![old config screen](./art/old-1.png)
![old training screen](./art/old-2.png)

- originally a dark themed App from the age of Android 4
- for this version, nearly everything of it was rewritten
  - new UI (most changes were made at the config screen)
  - Kotlin instead of Java
  - training logic separated from the UI
  - Room as database abstraction instead of using sqlite directly
  - object serialization using JSON instead of a custom format

### License

GPL-3.0-only
