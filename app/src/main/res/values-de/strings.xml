<?xml version="1.0" encoding="utf-8"?>
<!--
    Calculate Android Copyright <C> 2020 Jonas Lochmann
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation version 3 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see https://www.gnu.org/licenses/
-->
<resources>
    <string name="app_name">Rechnen</string>

    <string name="generic_cancel">Abbrechen</string>
    <string name="generic_save">Speichern</string>

    <string name="user_list_empty_text">Zum Benutzen dieser Anwendung muss ein Benutzer erstellt werden.
        Dieser wird nur lokal auf diesem Gerät gespeichert.</string>

    <string name="user_list_statistic">Letzte Benutzung: %1$s - %2$s</string>
    <string name="user_list_statistic_empty">noch nie verwendet</string>
    <string name="user_list_playtime">%s gespielt</string>

    <string name="time_less_than_one_minute">weniger als eine Minute</string>
    <string name="time_and">%s und %s</string>
    <plurals name="time_minute">
        <item quantity="one">%d Minute</item>
        <item quantity="other">%d Minuten</item>
    </plurals>
    <plurals name="time_hour">
        <item quantity="one">%d Stunde</item>
        <item quantity="other">%d Stunden</item>
    </plurals>

    <string name="user_list_add_user_name_placeholder">Ihr Name</string>
    <plurals name="user_list_blocks">
        <item quantity="one">ein Block</item>
        <item quantity="other">%d Blöcke</item>
    </plurals>

    <string name="user_list_edit_difficulty">Schwierigkeit einstellen</string>
    <string name="user_list_delete_user">Benutzer löschen</string>
    <string name="user_list_deleted_user">Benutzer gelöscht</string>
    <string name="user_list_delete_user_question">Möchten Sie den Benutzer %s löschen?</string>
    <string name="user_list_view_statistic">Statistik anzeigen</string>
    <string name="user_list_create_user">Benutzer erstellen</string>
    <string name="user_list_create_user_confirm_btn">Erstellen</string>
    <string name="user_list_created_user">Benutzer %1$s erstellt</string>
    <string name="user_list_create_user_no_name_error">Der Name kann nicht leer sein</string>

    <plurals name="training_result_mistakes">
        <item quantity="one">%d Fehler</item>
        <item quantity="other">%d Fehler</item>
    </plurals>
    <plurals name="training_result_seconds">
        <item quantity="one">%d Sekunde</item>
        <item quantity="other">%d Sekunden</item>
    </plurals>
    <plurals name="training_block_result_too_much_mistakes">
        <item quantity="one">%d Fehler ist zu viel</item>
        <item quantity="other">%d Fehler sind zu viel</item>
    </plurals>
    <string name="training_block_result_too_slow">Sie waren zu langsam\nSie haben %1$s gemacht in %2$s</string>
    <string name="training_block_result_perfect">Perfekt, keine Fehler</string>
    <plurals name="training_block_result_good">
        <item quantity="one">Ein Fehler ist in Ordnung</item>
        <item quantity="other">%d Fehler sind in Ordnung</item>
    </plurals>
    <string name="training_block_result_redo">Aufgaben wiederholen</string>
    <string name="training_block_result_new_tasks">Mit neuen Aufgaben fortsetzen</string>
    <string name="training_block_result_stop">Beenden</string>
    <string name="training_result_difficulty_tip">Die
        Schwierigkeit kann angepasst werden, indem in der Benutzerliste lange
        auf den entsprechenden Benutzer getippt wird.
    </string>

    <string name="difficulty_general_settings">Allgemein</string>
    <string name="difficulty_operation_addition">Addition</string>
    <string name="difficulty_operation_subtraction">Subtraktion</string>
    <string name="difficulty_operation_multiplication">Multiplikation</string>
    <string name="difficulty_operation_factorization">Primfaktorzerlegung</string>
    <string name="difficulty_operation_division">Division</string>
    <string name="difficulty_enable_ten_transgression">Zehnerüberschreitung</string>
    <string name="difficulty_allow_negative_results">Negative Ergebnisse erlauben</string>
    <string name="difficulty_allow_remainder">Aufgaben mit Rest aktivieren</string>
    <string name="difficulty_digits_of_first_number">Stellen der ersten Zahl</string>
    <string name="difficulty_digits_of_second_number">Stellen der zweiten Zahl</string>
    <string name="difficulty_profile">Schwierigkeits-Profil</string>
    <string name="difficulty_operation_disabled">Deaktiviert</string>
    <plurals name="difficulty_digit_counter">
        <item quantity="one">%d Stelle</item>
        <item quantity="other">%d Stellen</item>
    </plurals>
    <string name="difficulty_operation_multiplication_enabled">%s mal %s</string>
    <string name="difficulty_operation_addition_enabled">%s plus %s %s</string>
    <string name="difficulty_operation_subtraction_enabled">%s minus %s %s</string>
    <string name="difficulty_operation_factorization_enabled">von %d bis %d</string>
    <string name="difficulty_operation_division_enabled">bis zu %d/%d %s</string>
    <string name="difficulty_operation_first_number_digits">Die erste Zahl hat %s</string>
    <string name="difficulty_operation_second_number_digits">Die zweite Zahl hat %s</string>
    <string name="difficulty_operation_min_number">kleinste Zahl</string>
    <string name="difficulty_operation_max_number">größte Zahl</string>
    <string name="difficulty_operation_enable_switch">Aktivieren</string>
    <string name="difficulty_operation_cannot_disable_reason">Diese Operation muss aktiv bleiben,
        da immer mindestens eine Operation aktiviert sein muss</string>
    <string name="difficulty_max_divisor">Divisoren bis %d</string>
    <string name="difficulty_max_dividend">Dividenten bis %d</string>
    <string name="suffix_with_ten_transgression">mit Zehnerüberschreitung</string>
    <string name="suffix_without_ten_transgression">ohne Zehnerüberschreitung</string>
    <string name="suffix_with_negative_results">mit negativen Ergebnissen</string>
    <string name="suffix_with_remainder">mit Rest</string>
    <string name="suffix_without_remainder">ohne Rest</string>
    <string name="difficulty_profile_easy">Einfach</string>
    <string name="difficulty_profile_medium">Mittel</string>
    <string name="difficulty_profile_hard">Schwierig</string>
    <string name="difficulty_profile_custom">Benutzerdefiniert</string>
    <plurals name="difficulty_tasks_per_block">
        <item quantity="one">%d Aufgabe je Runde</item>
        <item quantity="other">%d Aufgaben je Runde</item>
    </plurals>
    <string name="difficulty_time_per_round">%s je Runde</string>

    <plurals name="difficulty_take_wrong_tasks">
        <item quantity="other">%d falsch beantwortete Aufgaben erneut abfragen</item>
        <item quantity="one">%d falsch beantwortete Aufgabe erneut abfragen</item>
    </plurals>
    <plurals name="difficulty_take_right_tasks">
        <item quantity="other">%d richtig beantwortete Aufgaben erneut abfragen</item>
        <item quantity="one">%d richtig beantwortete Aufgabe erneut abfragen</item>
    </plurals>
    <plurals name="difficulty_max_wrong_tasks">
        <item quantity="other">Neue Aufgaben stellen, wenn es nicht mehr als %d falsche Antworten gab</item>
        <item quantity="one">Neue Aufgaben stellen, wenn es nicht mehr als %d falsche Antwort gab</item>
    </plurals>
    <plurals name="difficulty_general_summary_tasks">
        <item quantity="other">%d Aufgaben</item>
        <item quantity="one">%d Aufgabe</item>
    </plurals>
    <plurals name="difficulty_general_summary_mistake">
        <item quantity="other">%d Fehler</item>
        <item quantity="one">%d Fehler</item>
    </plurals>
    <string name="difficulty_general_summary">%s; %s; %s; %d richtige, %d falsch beantwortete Aufgaben mitnehmen</string>
    <string name="difficulty_save_toast">Einstellungen gespeichert</string>

    <string name="about_title">Über diese Anwendung</string>
    <string name="about_contained_software_title">enthaltene Software</string>
    <string name="about_developer_key_title">Fingerabdruck des PGP-Schlüssels des Entwicklers</string>
    <string name="about_source_title">Quelltext</string>
    <string name="about_license_title">Lizenz</string>

    <plurals name="statistic_solved_tasks">
        <item quantity="one">%d Aufgabe bearbeitet</item>
        <item quantity="other">%d Aufgaben bearbeitet</item>
    </plurals>
    <string name="statistic_recent_title">Zuletzt gelöste Aufgaben</string>
    <string name="statistic_solved_right_percent">%d%% richtig gelöst</string>

    <string name="statistic_average_duration_per_correct_task_at">Durchschnitliche Dauer je korrekter %s</string>
    <string name="statistic_percentage_of_correctly_solved_tasks">Anteil der richtig gelösten Aufgaben bei der %s</string>

    <string name="statistic_source_before_this_session">Vor dieser Sitzung</string>
    <string name="statistic_source_this_session_before_this_round">Diese Sitzung vor dieser Runde</string>
    <string name="statistic_source_this_round">In dieser Runde</string>

    <string name="statistic_value_seconds">Sekunden</string>

    <string name="input_config_title">Eingabefeld konfigurieren</string>
    <string name="input_config_size">Eingabefeldgröße</string>
    <string name="input_config_type">Eingabefeldtyp</string>
    <string name="input_config_type_phone">Telefon</string>
    <string name="input_config_type_calculator">Taschenrechner</string>
    <string name="input_config_align_horizontal">Horizontale Ausrichtung</string>
    <string name="input_config_align_vertical">Vertikale Ausrichtung</string>
    <string name="input_config_confirm_button_location">OK-Button-Position</string>
    <string name="input_config_align_top">oben</string>
    <string name="input_config_align_bottom">unten</string>
    <string name="input_config_align_left">links</string>
    <string name="input_config_align_right">rechts</string>
    <string name="input_config_align_center">mitte</string>

    <string name="time_trial_mode">Schnellrechnen</string>
    <string name="time_trial_result_headline">%s gelöst, davon waren %s richtig</string>
</resources>
