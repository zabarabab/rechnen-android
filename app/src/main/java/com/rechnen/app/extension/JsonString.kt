/*
 * Calculate Android Copyright <C> 2020 Jonas Lochmann
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/
 */
package com.rechnen.app.extension

import android.util.JsonReader
import android.util.JsonWriter
import java.io.StringReader
import java.io.StringWriter

fun <R> String.useJsonReader(block: (JsonReader) -> R) = StringReader(this).use { stringReader ->
    JsonReader(stringReader).use { jsonReader ->
        block(jsonReader)
    }
}

fun writeJson(block: (JsonWriter) -> Unit): String = StringWriter().use { stringWriter ->
    JsonWriter(stringWriter).use { jsonWriter ->
        block(jsonWriter)
    }

    stringWriter.buffer.toString()
}