/*
 * Calculate Android Copyright <C> 2020 Jonas Lochmann
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/
 */
package com.rechnen.app.data

import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase

object DatabaseMigrations {
    val MIGRATE_TO_V2 = object: Migration(1, 2) {
        override fun migrate(database: SupportSQLiteDatabase) {
            // the structure was changed and it's no big loss
            database.execSQL("DELETE FROM saved_task")
        }
    }

    val MIGRATE_TO_V3 = object: Migration(2, 3) {
        override fun migrate(database: SupportSQLiteDatabase) {
            database.execSQL("ALTER TABLE `user` ADD COLUMN `play_time` INTEGER NOT NULL DEFAULT 0")
        }
    }

    val MIGRATE_TO_V4 = object: Migration(3, 4) {
        override fun migrate(database: SupportSQLiteDatabase) {
            database.execSQL("CREATE TABLE IF NOT EXISTS `statistic_task_result` (`user_id` INTEGER NOT NULL, `task_index` INTEGER NOT NULL, `type` TEXT NOT NULL, `correct` INTEGER NOT NULL, PRIMARY KEY(`user_id`, `task_index`))")
            database.execSQL("ALTER TABLE `user` ADD COLUMN `solved_tasks` INTEGER NOT NULL DEFAULT 0")
        }
    }

    val MIGRATE_TO_V5 = object: Migration(4, 5) {
        override fun migrate(database: SupportSQLiteDatabase) {
            database.execSQL("DROP TABLE `statistic_task_result`")
            database.execSQL("CREATE TABLE IF NOT EXISTS `statistic_task_result` (`user_id` INTEGER NOT NULL, `task_index` INTEGER NOT NULL, `type` TEXT NOT NULL, `correct` INTEGER NOT NULL, `duration` INTEGER NOT NULL, PRIMARY KEY(`user_id`, `task_index`))")
        }
    }

    val MIGRATE_TO_V6 = object: Migration(5, 6) {
        override fun migrate(database: SupportSQLiteDatabase) {
            database.execSQL("ALTER TABLE `user` ADD COLUMN `input_configuration` TEXT NOT NULL DEFAULT '{}'")
        }
    }
}