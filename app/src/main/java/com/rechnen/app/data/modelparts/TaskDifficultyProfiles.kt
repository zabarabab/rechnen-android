/*
 * Calculate Android Copyright <C> 2020 Jonas Lochmann
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/
 */
package com.rechnen.app.data.modelparts

import com.rechnen.app.R

object TaskDifficultyProfiles {
    val easy = TaskDifficulty(
            multiplication = MultiplicationConfig(enable = false),
            addition = AdditionConfig(
                    enable = true,
                    digitsOfFirstNumber = 1,
                    digitsOfSecondNumber = 1
            ),
            subtraction = SubtractionConfig(
                    enable = true,
                    digitsOfFirstNumber = 1,
                    digitsOfSecondNumber = 1,
                    enableTenTransgression = false
            )
    )

    val medium = TaskDifficulty(
            multiplication = MultiplicationConfig(
                    enable = true,
                    digitsOfFirstNumber = 1,
                    digitsOfSecondNumber = 1
            ),
            addition = AdditionConfig(
                    enable = true,
                    digitsOfFirstNumber = 2,
                    digitsOfSecondNumber = 1
            ),
            subtraction = SubtractionConfig(
                    enable = true,
                    digitsOfFirstNumber = 2,
                    digitsOfSecondNumber = 1
            )
    )

    val hard = TaskDifficulty(
            multiplication = MultiplicationConfig(
                    enable = true,
                    digitsOfFirstNumber = 1,
                    digitsOfSecondNumber = 2
            ),
            addition = AdditionConfig(
                    enable = true,
                    digitsOfFirstNumber = 2,
                    digitsOfSecondNumber = 2
            ),
            subtraction = SubtractionConfig(
                    enable = true,
                    digitsOfFirstNumber = 2,
                    digitsOfSecondNumber = 2
            )
    )

    val profilesAndLabels = listOf(
            easy to R.string.difficulty_profile_easy,
            medium to R.string.difficulty_profile_medium,
            hard to R.string.difficulty_profile_hard
    )
}