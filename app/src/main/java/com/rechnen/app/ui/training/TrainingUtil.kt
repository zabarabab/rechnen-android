/*
 * Calculate Android Copyright <C> 2020 Jonas Lochmann
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/
 */
package com.rechnen.app.ui.training

import android.os.SystemClock
import com.rechnen.app.data.Database
import com.rechnen.app.data.model.SavedTask
import com.rechnen.app.data.model.StatisticTaskResult
import com.rechnen.app.data.modelparts.InputConfiguration
import com.rechnen.app.data.modelparts.Task
import com.rechnen.app.extension.globalRandom
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.ReceiveChannel
import kotlinx.coroutines.channels.consume
import kotlinx.coroutines.channels.produce
import java.util.*
import kotlin.math.absoluteValue

object TrainingUtil {
    private fun now() = SystemClock.elapsedRealtime()

    fun start(
            input: ReceiveChannel<TrainingInputEvent>,
            database: Database,
            userId: Int,
            scope: CoroutineScope,
            mode: TrainingMode
    ): ReceiveChannel<TrainingState> = when (mode) {
        TrainingMode.Regular -> startRegular(input, database, userId, scope)
        TrainingMode.TimeTrail -> startTimeTrail(input, database, userId, scope)
    }

    private fun startRegular(
            input: ReceiveChannel<TrainingInputEvent>,
            database: Database,
            userId: Int,
            scope: CoroutineScope
    ): ReceiveChannel<TrainingState> = scope.produce {
        var blockCount = 0

        input.consume {
            val user = database.user().getUserByIdCoroutine(userId)!!

            val trainingStatisticItemsBeforeTheSession = TrainingStatisticItem.fromDatabase(database.statisticTaskResult().getItemsByUserIdCoroutine(userId), TrainingStatisticItemSource.BeforeThisSession)
            val statisticTaskResultsOfThisSession: MutableList<StatisticTaskResult> = mutableListOf()

            while (true) {
                // do round
                database.user().updateUserLastUseAndBlockCountCoroutine(
                        userId = userId,
                        timestamp = System.currentTimeMillis(),
                        blockCount = ++blockCount
                )

                val roundStartTime = now()
                var delayedTime = 0L

                val savedTasks = database.savedTask().getUserTasksCoroutine(userId).map { it.task }

                val tasks = if (savedTasks.isEmpty())
                    (1..user.difficulty.roundConfig.tasksPerRound).map { user.difficulty.generateTask(globalRandom) }
                else
                    savedTasks

                if (tasks.size != savedTasks.size) {
                    withContext(Dispatchers.IO) {
                        database.runInTransaction {
                            database.savedTask().deleteUserTasksSync(userId)

                            database.savedTask().addUserTasksSync(
                                    tasks.map {
                                        SavedTask(
                                                id = 0,
                                                userId = userId,
                                                task = it
                                        )
                                    }
                            )
                        }
                    }
                }

                val rightTasks = mutableListOf<Task>()
                val wrongTasks = mutableListOf<Task>()
                val statisticTaskResults = mutableListOf<StatisticTaskResult>()

                tasks.shuffled().forEachIndexed { index, task ->
                    val taskObject = TrainingState.ShowTask.RegularModeTask(
                            task = task,
                            currentQuestionIndex = index,
                            totalQuestionCounter = tasks.size,
                            state = TrainingShowTaskMode.WaitingForInput,
                            inputConfiguration = user.inputConfiguration
                    )

                    val taskStartTime = now()

                    send(taskObject)

                    lateinit var answer: TrainingInputEvent.Result
                    while (true) {
                        val value = input.receive()

                        if (value is TrainingInputEvent.Result) {
                            answer = value
                            break
                        }
                    }

                    val taskEndTime = now()

                    val valid = task.checkSolution(answer.values, answer.prefixEnabled)

                    statisticTaskResults.add(
                            StatisticTaskResult(
                                    userId = userId,
                                    type = task.type,
                                    correct = valid,
                                    duration = (taskEndTime - taskStartTime).coerceAtLeast(10),
                                    taskIndex = 0   // will be added later
                            )
                    )

                    if (valid) {
                        rightTasks.add(task)
                        send(taskObject.copy(state = TrainingShowTaskMode.FeedbackRight))

                        delay(500); delayedTime += 500
                    } else {
                        wrongTasks.add(task)
                        send(taskObject.copy(state = TrainingShowTaskMode.FeedbackWrong))

                        delay(1000); delayedTime += 1000
                    }
                }

                val roundEndTime = now()
                val roundDuration = roundEndTime - roundStartTime - delayedTime

                val mistakes = wrongTasks.size
                val mistakeCountOk = mistakes <= user.difficulty.roundConfig.maximalMistakesPerRound
                val timeOk = roundDuration <= user.difficulty.roundConfig.maximalTimePerRound
                val userGetsNewTasks = mistakeCountOk && timeOk

                val result = if (mistakeCountOk)
                    if (timeOk)
                        TrainingRoundEndState.EverythingGood
                    else
                        TrainingRoundEndState.TooSlow
                else
                    TrainingRoundEndState.TooMuchMistakes

                val nextTasks: List<Task>? = if (userGetsNewTasks) {
                    val nextTasks = mutableListOf<Task>()

                    rightTasks.shuffle()
                    wrongTasks.shuffle()

                    if (rightTasks.isNotEmpty()) {
                        nextTasks.addAll(
                                rightTasks.subList(0, user.difficulty.roundConfig.reAskRightInNextRound.coerceAtMost(rightTasks.size - 1))
                        )
                    }

                    if (wrongTasks.isNotEmpty()) {
                        nextTasks.addAll(
                                wrongTasks.subList(0, user.difficulty.roundConfig.reAskWrongInNextRound.coerceAtMost(wrongTasks.size - 1))
                        )
                    }

                    while (nextTasks.size < user.difficulty.roundConfig.tasksPerRound) {
                        nextTasks.add(user.difficulty.generateTask(globalRandom))
                    }

                    nextTasks.shuffle()

                    nextTasks
                } else null

                val statisticResultsBeforeThisRound = TrainingStatisticItem.fromDatabase(statisticTaskResultsOfThisSession, TrainingStatisticItemSource.ThisSessionBeforeThisRound)

                val newStatisticItemTaskResults: List<StatisticTaskResult> = withContext(Dispatchers.IO) {
                    database.runInTransaction <List<StatisticTaskResult>> {
                        var solvedTasks = database.user().getUserSolvedTasksSync(userId)!!

                        val newStatisticItemTaskResults = statisticTaskResults.map { item ->
                            item.copy(
                                    taskIndex = ++solvedTasks
                            )
                        }

                        database.statisticTaskResult().insertItemsSync(newStatisticItemTaskResults)
                        statisticTaskResultsOfThisSession.addAll(newStatisticItemTaskResults)

                        database.statisticTaskResult().cleanup(
                                userId = userId,
                                lastIndexToKeep = solvedTasks - 50
                        )

                        database.user().updateUserSolvedTasksSync(userId, solvedTasks)

                        if (nextTasks != null) {
                            database.savedTask().deleteUserTasksSync(userId)

                            database.savedTask().addUserTasksSync(
                                    nextTasks.map {
                                        SavedTask(
                                                id = 0,
                                                userId = userId,
                                                task = it
                                        )
                                    }
                            )
                        }

                        newStatisticItemTaskResults
                    }
                }

                val statisticOfThisRound = TrainingStatisticItem.fromDatabase(newStatisticItemTaskResults, TrainingStatisticItemSource.ThisRound)

                val statistic = TrainingStatisticItemByParameter.sortData(
                        TrainingStatisticItemByParameter.groupData(
                                trainingStatisticItemsBeforeTheSession + statisticOfThisRound + statisticResultsBeforeThisRound
                        )
                )

                send(
                        TrainingState.EndOfRound.RegularEndOfRound(
                                mistakes = mistakes,
                                time = roundDuration,
                                result = result,
                                statistic = statistic
                        )
                )

                while (true) {
                    val value = input.receive()

                    if (value is TrainingInputEvent.Continue) {
                        break
                    }
                }
            }
        }
    }

    private fun startTimeTrail(
            input: ReceiveChannel<TrainingInputEvent>,
            database: Database,
            userId: Int,
            scope: CoroutineScope
    ): ReceiveChannel<TrainingState> = scope.produce {
        input.consume {
            val user = database.user().getUserByIdCoroutine(userId)!!
            val configSeed = user.difficulty.hashCode()

            while (true) {
                val startWallClockTime = System.currentTimeMillis()
                val random = Random(startWallClockTime / (1000 * 60))
                val info = "Seed: ${configSeed.absoluteValue % 1000}-${random.nextInt(1000)}"

                fun generateTask() = user.difficulty.generateTask(random)

                for (i in 5 downTo 1) {
                    send(TrainingState.Countdown(
                            remainingSeconds = i,
                            info = info
                    ))

                    delay(1000)
                }

                val tasks = mutableListOf<TrainingState.EndOfRound.TimeTrailEndOfRound.TimeTrailSolvedTask>()
                val startTime = now()
                val totalTime = user.difficulty.roundConfig.maximalTimePerRound.toLong()
                val endTime = startTime + totalTime

                var timeOver = false

                do {
                    val task = generateTask()
                    val taskStartTime = now()

                    var taskFinished = false
                    var isFirstIteration = true

                    do {
                        if (now() >= endTime) {
                            taskFinished = true; timeOver = true
                        } else {
                            send(TrainingState.ShowTask.TimeTrailTask(
                                    task = task,
                                    totalTime = totalTime,
                                    inputConfiguration = user.inputConfiguration,
                                    elapsedTime = now() - startTime,
                                    clearInput = isFirstIteration
                            )); isFirstIteration = false

                            val userAction = withTimeoutOrNull(100) { input.receive() }

                            if (userAction is TrainingInputEvent.Result) {
                                taskFinished = true

                                tasks.add(
                                        TrainingState.EndOfRound.TimeTrailEndOfRound.TimeTrailSolvedTask(
                                                task = task,
                                                input = userAction,
                                                time = now() - taskStartTime
                                        )
                                )
                            }
                        }
                    } while (!taskFinished)
                } while (!timeOver)

                send(TrainingState.EndOfRound.TimeTrailEndOfRound(tasks))

                while (true) {
                    val value = input.receive()

                    if (value is TrainingInputEvent.Continue) {
                        break
                    }
                }
            }
        }
    }
}

sealed class TrainingInputEvent {
    data class Result(val values: List<Long>, val prefixEnabled: Boolean): TrainingInputEvent() {
        fun format(task: Task): String {
            val prefix = if (prefixEnabled) task.prefix!! else ""
            val items = values.mapIndexed { index, value ->
                val separator = if (index == 0) "" else task.separator ?: ""

                separator + value.toString()
            }.joinToString()

            return prefix + items
        }
    }

    object Continue: TrainingInputEvent()
}

sealed class TrainingState {
    sealed class ShowTask: TrainingState() {
        abstract val task: Task
        abstract val state: TrainingShowTaskMode
        abstract val inputConfiguration: InputConfiguration
        abstract val clearInput: Boolean

        data class RegularModeTask(
                override val task: Task,
                val currentQuestionIndex: Int,
                val totalQuestionCounter: Int,
                override val state: TrainingShowTaskMode,
                override val inputConfiguration: InputConfiguration
        ): ShowTask() {
            override val clearInput: Boolean = state == com.rechnen.app.ui.training.TrainingShowTaskMode.WaitingForInput
        }

        data class TimeTrailTask(
                override val task: Task,
                val elapsedTime: Long,
                val totalTime: Long,
                override val inputConfiguration: InputConfiguration,
                override val clearInput: Boolean
        ): ShowTask() {
            override val state = TrainingShowTaskMode.WaitingForInput
        }
    }

    sealed class EndOfRound(): TrainingState() {
        data class RegularEndOfRound(
                val mistakes: Int,
                val time: Long,
                val result: TrainingRoundEndState,
                val statistic: List<TrainingStatisticItemByParameter>
        ): EndOfRound()

        data class TimeTrailEndOfRound(
                // the bool indicates if it was solved correctly
                val tasks: List<TimeTrailSolvedTask>
        ): EndOfRound() {
            data class TimeTrailSolvedTask(
                    val task: Task,
                    val input: TrainingInputEvent.Result,
                    val time: Long
            ) {
                val correct = task.checkSolution(input.values, input.prefixEnabled)
            }
        }
    }

    data class Countdown(val remainingSeconds: Int, val info: String?): TrainingState() {
        init { if (remainingSeconds < 0) throw IllegalArgumentException() }
    }
}

enum class TrainingRoundEndState {
    EverythingGood,
    TooSlow,
    TooMuchMistakes
}

enum class TrainingShowTaskMode {
    WaitingForInput,
    FeedbackWrong,
    FeedbackRight
}

enum class TrainingMode {
    Regular,
    TimeTrail
}