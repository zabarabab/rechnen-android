/*
 * Calculate Android Copyright <C> 2020 Jonas Lochmann
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/
 */
package com.rechnen.app.ui.user

import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.FragmentManager
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.rechnen.app.R
import com.rechnen.app.Threads
import com.rechnen.app.data.AppDatabase
import com.rechnen.app.data.modelparts.InputConfiguration
import com.rechnen.app.databinding.InputConfigDialogBinding
import com.rechnen.app.extension.openParentScreen
import com.rechnen.app.extension.openSubscreen
import com.rechnen.app.extension.useJsonReader
import com.rechnen.app.extension.writeJson
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class EditUserInputConfigurationDialog: BottomSheetDialogFragment() {
    companion object {
        private const val DIALOG_TAG = "EditUserInputConfigurationDialog"
        private const val ARG_USER_ID = "userId"
        private const val STATE_INPUT_CONFIGURATION = "inputConfiguration"
        private const val STATE_SCREEN = "screen"

        private const val PAGE_OVERVIEW = 0
        private const val PAGE_LOADING = 1
        private const val PAGE_INPUT_TYPE = 2
        private const val PAGE_HORIZONTAL = 3
        private const val PAGE_VERTICAL = 4
        private const val PAGE_CONFIRM_BUTTON_LOCATION = 5

        private const val REQUEST_CHANGE_SIZE = 1

        fun newInstance(userId: Int) = EditUserInputConfigurationDialog().apply {
            arguments = Bundle().apply {
                putInt(ARG_USER_ID, userId)
            }
        }
    }

    private var didLoadInputConfiguration = false
    private lateinit var inputConfiguration: InputConfiguration
    private lateinit var binding: InputConfigDialogBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        savedInstanceState?.getString(STATE_INPUT_CONFIGURATION)?.let { inputConfigurationString ->
            inputConfiguration = inputConfigurationString.useJsonReader { InputConfiguration.parse(it) }
            didLoadInputConfiguration = true
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        outState.putInt(STATE_SCREEN, binding.flipper.displayedChild)

        if (didLoadInputConfiguration) {
            outState.putString(STATE_INPUT_CONFIGURATION, writeJson { inputConfiguration.serialize(it) })
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == REQUEST_CHANGE_SIZE && resultCode == Activity.RESULT_OK) {
            inputConfiguration = inputConfiguration.copy(
                    size = data!!.getIntExtra(ConfigureScaleFactorActivity.RESULT_EXTRA_FACTOR, -1)
            )

            bindSize()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val database = AppDatabase.with(context!!)
        val userId = arguments!!.getInt(ARG_USER_ID)

        binding = InputConfigDialogBinding.inflate(inflater, container, false)

        if (didLoadInputConfiguration) {
            bindAll()

            binding.flipper.displayedChild = savedInstanceState?.getInt(STATE_SCREEN, PAGE_OVERVIEW) ?: PAGE_OVERVIEW
        } else {
            binding.flipper.displayedChild = PAGE_LOADING

            GlobalScope.launch (Dispatchers.Main) {
                inputConfiguration = database.user().getUserByIdCoroutine(userId)!!.inputConfiguration
                didLoadInputConfiguration = true

                bindAll()
                binding.flipper.displayedChild = PAGE_OVERVIEW
            }
        }

        binding.keyboardTypeButton.setOnClickListener { binding.flipper.openSubscreen(PAGE_INPUT_TYPE) }
        binding.horizontalAlignButton.setOnClickListener { binding.flipper.openSubscreen(PAGE_HORIZONTAL) }
        binding.verticalAlignButton.setOnClickListener { binding.flipper.openSubscreen(PAGE_VERTICAL) }
        binding.keyboardSizeButton.setOnClickListener { startActivityForResult(ConfigureScaleFactorActivity.buildIntent(context!!, inputConfiguration), REQUEST_CHANGE_SIZE) }
        binding.confirmButtonLocationButton.setOnClickListener { binding.flipper.openSubscreen(PAGE_CONFIRM_BUTTON_LOCATION) }

        binding.saveButton.setOnClickListener {
            Threads.database.submit {
                database.user().updateUserInputConfigurationSync(userId, inputConfiguration)
            }

            Toast.makeText(context!!, R.string.difficulty_save_toast, Toast.LENGTH_SHORT).show()

            dismiss()
        }

        binding.type.let { typeBinding ->
            fun setType(type: InputConfiguration.KeyboardType) {
                inputConfiguration = inputConfiguration.copy(type = type)
                bindKeyboardType()
                binding.flipper.openParentScreen(PAGE_OVERVIEW)
            }

            typeBinding.calculator.setOnClickListener { setType(InputConfiguration.KeyboardType.Calculator) }
            typeBinding.phone.setOnClickListener { setType(InputConfiguration.KeyboardType.Phone) }
        }

        binding.horizontal.let { typeBinding ->
            fun setType(horizontalAlignment: InputConfiguration.HorizontalAlignment) {
                inputConfiguration = inputConfiguration.copy(horizontal = horizontalAlignment)
                bindHorizontal()
                binding.flipper.openParentScreen(PAGE_OVERVIEW)
            }

            typeBinding.left.setOnClickListener { setType(InputConfiguration.HorizontalAlignment.Left) }
            typeBinding.center.setOnClickListener { setType(InputConfiguration.HorizontalAlignment.Center) }
            typeBinding.right.setOnClickListener { setType(InputConfiguration.HorizontalAlignment.Right) }
        }

        binding.vertical.let { typeBinding ->
            fun setType(verticalAlignment: InputConfiguration.VerticalAlignment) {
                inputConfiguration = inputConfiguration.copy(vertical = verticalAlignment)
                bindVertical()
                binding.flipper.openParentScreen(PAGE_OVERVIEW)
            }

            typeBinding.top.setOnClickListener { setType(InputConfiguration.VerticalAlignment.Top) }
            typeBinding.center.setOnClickListener { setType(InputConfiguration.VerticalAlignment.Center) }
            typeBinding.bottom.setOnClickListener { setType(InputConfiguration.VerticalAlignment.Bottom) }
        }

        binding.confirmButtonLocation.let { typeBinding ->
            fun setType(confirmButtonLocation: InputConfiguration.ConfirmButtonLocation) {
                inputConfiguration = inputConfiguration.copy(confirmButtonLocation = confirmButtonLocation)
                bindConfirmButtonLocation()
                binding.flipper.openParentScreen(PAGE_OVERVIEW)
            }

            typeBinding.left.setOnClickListener { setType(InputConfiguration.ConfirmButtonLocation.Left) }
            typeBinding.right.setOnClickListener { setType(InputConfiguration.ConfirmButtonLocation.Right) }
        }

        return binding.root
    }

    private fun bindAll() {
        bindKeyboardType()
        bindSize()
        bindHorizontal()
        bindVertical()
        bindConfirmButtonLocation()
    }

    private fun bindKeyboardType() {
        binding.keyboardTypeButton.text = when (inputConfiguration.type) {
            InputConfiguration.KeyboardType.Phone -> getString(R.string.input_config_type_phone)
            InputConfiguration.KeyboardType.Calculator -> getString(R.string.input_config_type_calculator)
        }

        binding.type.phone.isChecked = inputConfiguration.type == InputConfiguration.KeyboardType.Phone
        binding.type.calculator.isChecked = inputConfiguration.type == InputConfiguration.KeyboardType.Calculator
    }

    private fun bindSize() {
        binding.keyboardSizeButton.text = "${inputConfiguration.size}%"
    }

    private fun bindHorizontal() {
        binding.horizontalAlignButton.text = when (inputConfiguration.horizontal) {
            InputConfiguration.HorizontalAlignment.Left -> getString(R.string.input_config_align_left)
            InputConfiguration.HorizontalAlignment.Center -> getString(R.string.input_config_align_center)
            InputConfiguration.HorizontalAlignment.Right -> getString(R.string.input_config_align_right)
        }

        binding.horizontal.left.isChecked = inputConfiguration.horizontal == InputConfiguration.HorizontalAlignment.Left
        binding.horizontal.center.isChecked = inputConfiguration.horizontal == InputConfiguration.HorizontalAlignment.Center
        binding.horizontal.right.isChecked = inputConfiguration.horizontal == InputConfiguration.HorizontalAlignment.Right
    }

    private fun bindVertical() {
        binding.verticalAlignButton.text = when (inputConfiguration.vertical) {
            InputConfiguration.VerticalAlignment.Top -> getString(R.string.input_config_align_top)
            InputConfiguration.VerticalAlignment.Center -> getString(R.string.input_config_align_center)
            InputConfiguration.VerticalAlignment.Bottom -> getString(R.string.input_config_align_bottom)
        }

        binding.vertical.top.isChecked = inputConfiguration.vertical == InputConfiguration.VerticalAlignment.Top
        binding.vertical.center.isChecked = inputConfiguration.vertical == InputConfiguration.VerticalAlignment.Center
        binding.vertical.bottom.isChecked = inputConfiguration.vertical == InputConfiguration.VerticalAlignment.Bottom
    }

    private fun bindConfirmButtonLocation() {
        binding.confirmButtonLocationButton.text = when (inputConfiguration.confirmButtonLocation) {
            InputConfiguration.ConfirmButtonLocation.Left -> getString(R.string.input_config_align_left)
            InputConfiguration.ConfirmButtonLocation.Right -> getString(R.string.input_config_align_right)
        }

        binding.confirmButtonLocation.left.isChecked = inputConfiguration.confirmButtonLocation == InputConfiguration.ConfirmButtonLocation.Left
        binding.confirmButtonLocation.right.isChecked = inputConfiguration.confirmButtonLocation == InputConfiguration.ConfirmButtonLocation.Right
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog = object: BottomSheetDialog(context!!, theme) {
        override fun onBackPressed() {
            if (binding.flipper.displayedChild != PAGE_OVERVIEW && binding.flipper.displayedChild != PAGE_LOADING) {
                binding.flipper.openParentScreen(PAGE_OVERVIEW)
            } else {
                super.onBackPressed()
            }
        }
    }

    fun show(fragmentManager: FragmentManager) = show(fragmentManager, DIALOG_TAG)
}