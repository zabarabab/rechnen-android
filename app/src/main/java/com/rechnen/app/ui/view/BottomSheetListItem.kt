/*
 * Calculate Android Copyright <C> 2020 Jonas Lochmann
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/
 */

package com.rechnen.app.ui.view

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.FrameLayout
import android.widget.TextView
import com.rechnen.app.R

class BottomSheetListItem(context: Context, attributeSet: AttributeSet): FrameLayout(context, attributeSet) {
    init {
        inflate(context, R.layout.bottom_sheet_list_item, this)
    }

    private val titleView = findViewById<TextView>(R.id.title).apply { visibility = View.GONE }
    private val textView = findViewById<TextView>(R.id.text).apply { visibility = View.GONE }

    var title: String?
        get() = titleView.text.toString()
        set(value) {
            titleView.text = value
            titleView.visibility = if (value.isNullOrBlank()) View.GONE else View.VISIBLE
        }

    var text: String?
        get() = textView.text.toString()
        set(value) {
            textView.text = value
            textView.visibility = if (value.isNullOrBlank()) View.GONE else View.VISIBLE
        }

    init {
        val attributes = context.obtainStyledAttributes(attributeSet, R.styleable.BottomSheetListItem)

        title = attributes.getString(R.styleable.BottomSheetListItem_itemTitle)
        text = attributes.getString(R.styleable.BottomSheetListItem_itemText)

        attributes.recycle()
    }
}