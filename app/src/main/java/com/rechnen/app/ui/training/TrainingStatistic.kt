/*
 * Calculate Android Copyright <C> 2020 Jonas Lochmann
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/
 */

package com.rechnen.app.ui.training

import com.rechnen.app.data.model.StatisticTaskResult
import com.rechnen.app.data.modelparts.TaskType

data class TrainingStatisticItem(
        val taskType: TaskType,
        val parameter: TrainingStatisticItemParameter,
        val source: TrainingStatisticItemSource,
        val value: Int
) {
    companion object {
        fun fromDatabase(items: List<StatisticTaskResult>, source: TrainingStatisticItemSource): List<TrainingStatisticItem> {
            val result = mutableListOf<TrainingStatisticItem>()

            val allTaskTypes = items.map { it.type }.distinct()
            val correctTaskTypes = items.filter { it.correct }.map { it.type }.distinct()

            allTaskTypes.forEach { type ->
                val relatedTasks = items.filter { it.type == type }

                result.add(
                        TrainingStatisticItem(
                                parameter = TrainingStatisticItemParameter.PercentageOfCorrectlySolvedTasks,
                                source = source,
                                value = relatedTasks.count { it.correct } * 100 / relatedTasks.size,
                                taskType = type
                        )
                )
            }

            correctTaskTypes.forEach { type ->
                val relatedTasks = items.filter { it.correct && it.type == type }

                var totalDuration = 0L; relatedTasks.forEach { totalDuration += it.duration }

                result.add(
                        TrainingStatisticItem(
                                parameter = TrainingStatisticItemParameter.AverageDurationPerCorrectTask,
                                source = source,
                                value = (totalDuration / relatedTasks.size.toLong()).coerceAtMost(Int.MAX_VALUE.toLong()).toInt(),
                                taskType = type
                        )
                )
            }

            return result
        }
    }
}

data class TrainingStatisticItemByParameter(
        val taskType: TaskType,
        val parameter: TrainingStatisticItemParameter,
        val values: Map<TrainingStatisticItemSource, Int>
) {
    companion object {
        fun groupData(items: List<TrainingStatisticItem>): List<TrainingStatisticItemByParameter> {
            return items.groupBy { it.taskType to it.parameter }.map { (taskTypeAndParameter, itemsByParameter) ->
                TrainingStatisticItemByParameter(
                        taskType = taskTypeAndParameter.first,
                        parameter = taskTypeAndParameter.second,
                        values = mutableMapOf<TrainingStatisticItemSource, Int>().also { map ->
                            itemsByParameter.forEach {
                                map[it.source] = it.value
                            }
                        }
                )
            }
        }

        fun sortData(items: List<TrainingStatisticItemByParameter>): List<TrainingStatisticItemByParameter> = items.sortedByDescending { it.ranking }
    }

    init {
        if (values.isEmpty()) {
            throw IllegalArgumentException()
        }
    }

    private val inThisRound: Int? = values[TrainingStatisticItemSource.ThisRound]
    private val beforeThisRound: Int? = values[TrainingStatisticItemSource.ThisSessionBeforeThisRound]
    private val beforeThisSession: Int? = values[TrainingStatisticItemSource.BeforeThisSession]
    private val compareBy: Int? = beforeThisRound ?: beforeThisSession

    val ranking: Int = if (inThisRound == null)
        Int.MIN_VALUE
    else if (compareBy == null)
        Int.MAX_VALUE
    else {
        val diff = inThisRound - compareBy

        when (parameter) {
            TrainingStatisticItemParameter.AverageDurationPerCorrectTask -> -diff
            TrainingStatisticItemParameter.PercentageOfCorrectlySolvedTasks -> (100L * diff).coerceAtLeast(Int.MIN_VALUE.toLong()).coerceAtMost(Int.MAX_VALUE.toLong()).toInt()
        }
    }

    val change: TrainingStatisticItemChange? = if (inThisRound == null || compareBy == null)
        null
    else if (ranking > 100)
        TrainingStatisticItemChange.Better
    else if (ranking < -100)
        TrainingStatisticItemChange.Worser
    else
        TrainingStatisticItemChange.Equal
}

enum class TrainingStatisticItemParameter {
    AverageDurationPerCorrectTask,
    PercentageOfCorrectlySolvedTasks
}

enum class TrainingStatisticItemSource {
    ThisRound,
    ThisSessionBeforeThisRound,
    BeforeThisSession
}

object TrainingStatisticItemSourceSorting {
    fun rank(item: TrainingStatisticItemSource) = when (item) {
        TrainingStatisticItemSource.BeforeThisSession -> 0
        TrainingStatisticItemSource.ThisSessionBeforeThisRound -> 1
        TrainingStatisticItemSource.ThisRound -> 2
    }
}

enum class TrainingStatisticItemChange {
    Better,
    Equal,
    Worser
}